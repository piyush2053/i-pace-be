const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql2');
const tables = require('./Table_Constants');
const dotenv = require('dotenv')
dotenv.config();

const app = express();

app.use(cors());
app.use(bodyparser.json());

app.listen(6060, () => {
    console.log('BE Running at Port 3000');
})

//connection to Database 
const db = mysql.createConnection({
    host: process.env.DBHOST,
    user: process.env.DBUSER,
    password: process.env.DBPASSWORD,
    database: process.env.DBDATABASE,
    port: process.env.DBPORT
})

//checking connection with Database
db.connect(err => {
    if (err) {
        console.log("404 - Error Connecting to Database")
    } else {
        console.log("Database Connection Successful !")
    }
})

app.post("/certficateRegister", (req, res) => {
    let courseName = req.body.courseName;
    let courseDes = req.body.courseDes;
    let courseRemarks = req.body.courseRemarks;
    let certificateFile = req.body.certificateFile;
    console.log("Api running to Post certificate deatils in Database");
    let qrr = `INSERT INTO ${tables.TABLE_CERTIFICATION} VALUES ("WD/191/IND", "Piyush Patel", "piyush.patel@rws.com", "${courseName}", "${courseDes}","${courseRemarks}", "${certificateFile}");`;
    console.log(qrr)
    db.query(qrr, (err, results) => {
        if (err) {
            console.log("Error", err);
        } else {
            res.send({
                message: "Records Pushed into DB",
                data: results
            });
        }
    });
})
